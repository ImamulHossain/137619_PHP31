<?php
include_once('../../vendor/autoload.php');
session_start();
use App\Users\User;
use App\Users\Auth;
use App\Message\Message;
use App\Utility\Utility;

$auth= new Auth();
$status= $auth->logout();
if($status){
    Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> You have successfully logged out
</div>");
        Utility::redirect('../../index.php');
    }


