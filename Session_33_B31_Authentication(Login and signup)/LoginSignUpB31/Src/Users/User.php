<?php

namespace App\Users;
include_once('../../vendor/autoload.php');
use App\Model\Database as DB;




class User extends DB
{
    public $id = "";
    public $first_name = "";
    public $last_name = "";
    public $email = "";
    public $password = "";
    public $token;



public function setData($data = Array())
{
    if (array_key_exists('first_name', $data)) {
        $this->first_name = $data['first_name'];
    }
    if (array_key_exists('last_name', $data)) {
        $this->last_name = $data['last_name'];
    }
    if (array_key_exists('email', $data)) {
        $this->email = $data['email'];
    }
    if (array_key_exists('password', $data)) {
        $this->password = md5($data['password']);
    }
    if (array_key_exists('token', $data)) {
        $this->token = ($data['token']);
    }
    if (array_key_exists('id', $data)) {
        $this->id = $data['id'];
    }
    return $this;
    //var_dump($this);
    //die();
}



    public function __construct()
    {
        parent::__construct();
    }


    public function store(){
        $query = "INSERT INTO `loginsignupb31`.`users` (`first_name`,`last_name`, `email`, `password`,`token`) VALUES (:first_name,:last_name,:email,:password,:token)";
        $stmt = $this->conn->prepare($query);
        $result = $stmt->execute(array(":first_name" => $this->first_name,
            ":last_name" => $this->last_name,
            ":email" => $this->email,
            ":password" => $this->password,
            ":token" => $this->token));
    }




}

