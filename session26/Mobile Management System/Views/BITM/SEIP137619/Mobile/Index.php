<?php
include($_SERVER['DOCUMENT_ROOT'] . "/137619_PHP31/session26/Mobile Management System/vendor/autoload.php");
session_start();
use App\BITM\SEIP137619\Mobile\Mobile;
use App\BITM\SEIP137619\Message\Message;

$mobile = new Mobile();
$allResult = $mobile->index();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Mobile</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <br><br>
    <a href="Create.php" class="btn btn-primary" role="button">Add Data</a>
    <div id="message">
        <?php
        if (array_key_exists('giveMessage', $_SESSION) and !empty($_SESSION['giveMessage'])) {
            echo Message::Message();
        }
        ?>
    </div>


    <h2>Mobile Informations:</h2>
    <table class="table table-hover table-striped table-bordered">
        <thead>
        <tr bgcolor="#5f9ea0">
            <th>SL.</th>
            <th>ID</th>
            <th>Mobile Brand</th>
            <th>Created By</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>

        <?php
        $sl = 0;
        foreach ($allResult as $data) {
            //$sl++;
            ?>

            <tr>
                <td><?php echo $sl++; ?></td>
                <td><?php echo $data['id']; ?></td>
                <td><?php echo $data['mobileTitle']; ?></td>
                <td><?php echo $data['createdBy']; ?></td>
                <td>
                    <a href="View.php?id=<?php echo $data['id']; ?>" class="btn btn-info btn-xs" role="button">View</a>
                    <a href="Edit.php" class="btn btn-warning btn-xs" role="button">Edit</a>
                    <a href="Delete.php" class="btn btn-danger btn-xs" role="button">Delete</a>
                </td>
            </tr>

        <?php } ?>

        </tbody>
    </table>
</div>
<script>
    $("#message").show().delay(3000).fadeOut();
</script>

</body>
</html>
