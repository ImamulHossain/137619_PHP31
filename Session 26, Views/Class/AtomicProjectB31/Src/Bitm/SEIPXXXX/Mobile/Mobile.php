<?php
namespace App\Bitm\SEIPXXXX\Mobile;
use App\Bitm\SEIPXXXX\Utility\Utility;
use PDO;
use App\Bitm\SEIPXXXX\Message\Message;
class Mobile{
    public $id="";
    public $title="";
    public $model_name="";
    public $serverName="localhost";
    public $databaseName="atomicproject";
    public $user="root";
    public $pass="";
    public $conn;

    public function __construct()
    {
        try {
            # MySQL with PDO_MYSQL
            $this->conn = new PDO("mysql:host=$this->serverName;dbname=$this->databaseName", $this->user, $this->pass);
        }
        catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function setData($data=""){
        if(array_key_exists('title',$data) and !empty($data)){
            $this->title=$data['title'];
        }
        if(array_key_exists('model_name',$data) and !empty($data)){
            $this->model_name=$data['model_name'];
        }
        return $this;
    }
    public function store(){
        $query="INSERT INTO `mobile` (`title`, `modeddl_name`) VALUES (:joty, :model)";
        $stmt=$this->conn->prepare( $query);
        $result=$stmt->execute(array(':joty'=>$this->title,
            ':model'=>$this->model_name
        ));
        if($result){
            //header('Location:index.php');
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been stored successfully.
</div>");
            Utility::redirect('index.php');
        }
        else{
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Some error has been occured
</div>");
            Utility::redirect('index.php');
        }

    }



    public function index(){
       $sqlquery="SELECT * FROM `mobile`";
        $stmt=$this->conn->query($sqlquery);
        $alldata=$stmt->fetchAll(PDO::FETCH_OBJ);
        return $alldata;
    }


}


























