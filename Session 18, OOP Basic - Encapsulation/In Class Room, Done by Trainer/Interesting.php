<?php

class Interesting
{
    public function __construct($a = '', $b = '')
    {
        echo $a * $b;

    }

    public function funny()
    {
        echo "This output from funny method of Interesting Class<br/>";
    }
}

class Google extends Interesting
{
    public function funny()
    {
        echo "This output from funny method of Google Class";
    }
}

class Yahoo extends Google
{
    public function __construct($a, $b)
    {
        echo $a + $b . "<br/>";
        parent::__construct($a, $b);
    }

    public function sunny()
    {
        echo "SUN";
    }

}



//$amarObject = new Interesting();
//$amarObject->funny();


//$subClassErObject = new Google();
//$subClassErObject->funny();












