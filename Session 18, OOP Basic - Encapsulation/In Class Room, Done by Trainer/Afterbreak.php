<?php

class Afterbreak
{
    protected function abc()
    {
        echo "I'M FROM ABC protected method of Afterbreak class</br>";

    }

    public function xyz()
    {
        $this->abc();
    }
}

class SecondClass extends Afterbreak
{
    public function secMethod()
    {
        $o = new Afterbreak();
        $o->abc();
        $this->abc();
    }

}