<?php
echo "<b>Relational Database:</b> Relations between the tables are called relational database. How the tables are managed to each other can understood by relational database. There are 3 types of relations.<br>";
echo "<b>Many to One:</b>If we consider two tables Student and Department, then a student can admit in one department only, so there is a relation of many to one relation.<br>";
echo "<b>One to One:</b>If we consider two tables Student and HisFather, then a student has only one father only, so there is a relation of one to one relation.<br>";
echo "<b>Many to Many:</b> If we consider student and teacher table then a teacher has many students and a student has many teachers, so the relation between them is many to many.<br>";

