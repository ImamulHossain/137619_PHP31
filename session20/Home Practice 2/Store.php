<?php
include('vendor/autoload.php');
use App\PersonalBio\Name\Name;
use App\PersonalBio\Contact\Contact;
use App\PersonalBio\Bdate\Bdate;
use App\PersonalBio\Image\Image;
$name= new Name($_POST['fname'], $_POST['lname']);
$con=new Contact($_POST['email'],$_POST['mobile'] );
$dob=new Bdate($_POST['bdate']);
$phtoo=new Image($_POST['photo']);
