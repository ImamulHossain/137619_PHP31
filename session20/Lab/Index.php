<?php
include('vendor/autoload.php');

use App\SEIP\ID137619\Module\About\About;
use App\SEIP\ID137619\Module\Contact\Contact;
use App\SEIP\ID137619\Module\Image\Image;
use App\SEIP\ID137619\Module\Profile\Profile;
use App\SEIP\ID137619\Module\Users\Users;

$obj1 = new About('Imamul Hossain');
$obj2 = new Contact('01923080348');
$obj3 = new Image('IMAGE');
$obj4 = new Profile("BASIS");
$obj5 = new Users('137619');
