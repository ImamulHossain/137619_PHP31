<?php
class Calculator
{
    private $number1='';
    private $number2='';
    public function Add(){
        echo $this->number1 + $this->number2;
    }
    public function Sub(){
        echo $this->number1 - $this->number2;
    }
    public function Mul(){
        echo $this->number1 * $this->number2;
    }
    public function Div(){
        echo $this->number1 / $this->number2;
    }
    public function setData($num1, $num2){
        $this->number1=$num1;
        $this->number2=$num2;
    }
    public function getData(){
        if($_POST['result']=='Add'){
            $this->Add();
        }
        elseif($_POST['result']=='Sub'){
            $this->Sub();
        }
        elseif($_POST['result']=='Mul'){
            $this->Mul();
        }
        else{
            if ($_POST['num2']==0){
                echo 'Divisor can not be zero.';
            }
            else{
                $this->Div();
            }
        }
    }
}