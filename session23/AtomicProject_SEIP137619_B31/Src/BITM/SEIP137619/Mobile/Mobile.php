<?php

namespace App\BITM\SEIP137619\Mobile;
class Mobile
{
    public $id = '';
    public $mobileTitle = '';
    public $created = '';
    public $modified = '';
    public $created_by = '';
    public $modified_by = '';
    public $deleted_at = '';

    public function __construct($model = false)
    {
        echo "I'm constructing data<br>";
    }

    public function index()
    {
        return "I'm indexing files.<br>";
    }

    public function create()
    {
        return "I'm creating data.<br>";
    }

    public function store()
    {
        return "I'm storing data.<br>";
    }

    public function edit()
    {
        return "I'm editing data.<br>";
    }

    public function update()
    {
        return "I'm updating data.<br>";
    }

    public function delete()
    {
        return "I'm deleting data.<br>";
    }

}