<?php
include("../../../../vendor/autoload.php");
session_start();
use App\BITM\SEIP137619\Email\Email;
use App\BITM\SEIP137619\Message\Message;

$email = new Email();
$result = $email->index();

?>


<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <br><br>
    <a href="Create.php" role="button" class="btn btn-primary">Add Email</a>
    <h2>Email</h2>
    <div id="message">
        <?php
        if(array_key_exists('giveMessage', $_SESSION) and !empty($_SESSION['giveMessage'])){
            echo Message::Message();
        }
        ?></div>
    <table class="table  table-bordered table-striped table-hover">
        <thead>
        <tr bgcolor="#7fffd4">
            <th>SL</th>
            <th>ID</th>
            <th>Name</th>
            <th>Email</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $sl = 0;
        foreach ($result as $data) {
            $sl++;
            ?>
            <tr>
                <td><?php echo $sl; ?></td>
                <td><?php echo $data['id']; ?></td>
                <td><?php echo $data['name']; ?></td>
                <td><?php echo $data['email']; ?></td>
                <td>
                    <a href="View.php?id=<?php echo $data['id']; ?>" class="btn btn-info btn-xs" role="button">View</a>
                    <a href="Edit.php" class="btn btn-warning btn-xs" role="button">Edit</a>
                    <a href="Delete.php" class="btn btn-danger btn-xs" role="button">Delete</a>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
<script>
    $("#message").show().delay(3000).fadeOut();
</script>
</body>
</html>