<?php
namespace App\BITM\SEIP137619\Email;


use PDO;
use App\BITM\SEIP137619\Message\Message;

class Email
{
    public $id = "";
    public $email = "";
    public $name = "";
    public $serverName = "localhost";
    public $dabName = "mobilemanagementsystem";
    public $user = "basis";
    public $pass = "bitm";
    public $conn;

    public function __construct()
    {
        try {
            $hostName = $this->serverName;
            $dbName = $this->dabName;
            $user = $this->user;
            $pass = $this->pass;
            $this->conn = new PDO("mysql:host=$hostName; dbname=$dbName", $user, $pass);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function setData($data = "")
    {
        if (array_key_exists('name', $data) and !empty($data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('email', $data) and !empty($data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('id', $data) and !empty($data)) {
            $this->id = $data['id'];
        }
        return $this;
    }

    public function index()
    {
        $sql = "SELECT * FROM `email`";
        $getSql = $this->conn->query($sql);
        $allResult = $getSql->fetchAll(PDO::FETCH_ASSOC);
        return $allResult;
    }

    public function store()
    {
        $sql = "INSERT INTO `email`(`name`, `email`) VALUES (:myName, :myEmail)";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(':myName' => $this->name, ':myEmail' => $this->email));
        if ($result) {
            Message::Message("<div class=\"alert alert-success\">
                                        <strong>Success!</strong> Data is inserted successfully.
                              </div>");
            header("Location: Index.php");
        } else {
            Message::Message("<div class=\"alert alert-danger\">
                                        <strong>Warning!</strong> Data is not inserted.
                              </div>");
            header("Location: Index.php");
        }

    }

    public function view()
    {
        $sql = "SELECT * FROM `email` WHERE `id`=:setID";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(':setID' => $this->id));
        $singleResult = $getSql->fetch(PDO::FETCH_ASSOC);
        return $singleResult;
    }
}