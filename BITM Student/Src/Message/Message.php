<?php

namespace App\Message;
if(!isset($_SESSION['giveMessage'])){
    session_start();
}
class Message
{
    public static function message($message = NULL)
    {
        if (is_null($message)) {
            $msg = self::getMessage();
            return $msg;
        } else {
            return self::setMessage($message);
        }
    }

    public static function setMessage($message)
    {
        $_SESSION['giveMessage']=$message;
    }
    public static function getMessage()
    {
        $message=$_SESSION['giveMessage'];
        $_SESSION['giveMessage']='';
        return $message;
    }
}