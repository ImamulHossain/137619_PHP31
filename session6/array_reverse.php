<?php
$array = array('a' => 1, 'b'=> 'Imamul', array('c' => 2,'d'=> 3, 4, 'bitm'), 5, 'pondit');
echo "<pre>";
print_r($array);
$reverse = array_reverse($array);			// Reverses element with new associated key. 
print_r($reverse);
echo "<br>----------------------------------------";
$preserved = array_reverse($array, true);		// Reverses element keeping associated key of input array. 
print_r($preserved);