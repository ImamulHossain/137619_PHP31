
<?php
$MainArray = array("orange", "banana", "apple", "raspberry", array('sour' => 'mango', 'pineapple' ));
echo "<b>Input Array: </b>";
print_r($MainArray);

echo "<br><br>";
$delteted = array_pop($MainArray);
if($delteted){
	echo print_r($delteted).' has been <b>deleted</b>.';
}
else{
	echo "Nothing is deleted.";
}

echo "<br><br><b>Output Array: </b>";
print_r($MainArray);		// Main array after deleting last element.
?>
