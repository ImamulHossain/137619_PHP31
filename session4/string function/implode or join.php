
<?php

$array = array('lastname', 'email', 'phone');
$comma_separated = implode(",", $array);

echo $comma_separated;

// Empty string when using an empty array:
var_dump(implode(' ', array())); // string(0) ""
//but if the array has string(s)
echo "<br>";
var_dump(implode('something may be present here', array("imamul"))); // string(6) ""

echo "...........................................";
echo "<br>";
$elements = array('a', 'b', 'c');

echo "<ul><li>" . implode("</li><li>", $elements) . "</li></ul>";
echo "...........................................";
echo "<br>";
var_dump(implode(',',array(true, true, false, false, true)));        //TRUE became "1", FALSE becomes nothing. so it'll print 1,1,,,1.

echo "...........................................";
echo "<br>";

$id_nums = array(1,6,12,18,24);

$id_nums = implode(", ", $id_nums);

$sqlquery = "Select name,email,phone FROM usertable WHERE user_id IN ($id_nums)";
echo $sqlquery;
// join() is the alias of implode()

?>



