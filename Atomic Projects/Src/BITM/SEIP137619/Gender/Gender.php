<?php
namespace App\BITM\SEIP137619\Gender;
use App\BITM\SEIP137619\Message\Message;
use PDO;

class Gender
{
    public $id = "";
    public $name = "";
    public $gender = "";
    public $host = "localhost";
    public $db = "atomicprojects";
    public $user = "atomic";
    public $pass = "123";
    public $conn;

    public function __construct()
    {
        try {
            $this->conn = new PDO("mysql:host=$this->host;dbname=$this->db", $user = $this->user, $pass = $this->pass);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function setData($data = "")
    {
        if (array_key_exists('name', $data) and !empty($data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('gender', $data) and !empty($data)) {
            $this->gender = $data['gender'];
        }
        if (array_key_exists('id', $data) and !empty($data)) {
            $this->id = $data['id'];
        }
    }

    public function store()
    {
        $sql = "INSERT INTO `gender`(`name`, `gender`) VALUES (:myName, :myGender)";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(":myName" => $this->name, ":myGender" => $this->gender));
        if ($result) {
            Message::Message("<div class=\"alert alert-success\">
                                        <strong>Success!</strong> Data is inserted successfully.
                                   </div>");
            header("Location: index.php");
        } else {
            Message::Message("<div class=\"alert alert-danger\">
                                        <strong>Warning!</strong> Data is not inserted successfully.
                                   </div>");
            header("Location: index.php");
        }
    }

    public function index()
    {
        $sql = "SELECT * FROM `gender` WHERE `isTrash`=0";
        $getSql = $this->conn->query($sql);
        $result = $getSql->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function view()
    {
        $sql = "SELECT * FROM `gender` WHERE `id`=:getId";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(":getId" => $this->id));
        $singleResult = $getSql->fetch(PDO::FETCH_ASSOC);
        return $singleResult;
    }

    public function update()
    {
        $sql = "UPDATE `gender` SET `name`=:setName,`gender`=:setGender WHERE `gender`.`id`=:id";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(":setName" => $this->name, ":setGender" => $this->gender, ":id" => $this->id));
        if ($result) {
            Message::Message("<div class=\"alert alert-success\">
                                        <strong>Success!</strong> Data is updated successfully.
                                   </div>");
            header("Location: index.php");
        } else {
            Message::Message("<div class=\"alert alert-danger\">
                                        <strong>Warning!</strong> Error Occurred.
                                   </div>");
            header("Location: index.php");
        }

    }
    public function delete(){
        $sql="DELETE FROM `gender` WHERE `id`=:id";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(":id" => $this->id));
        if ($result) {
            Message::Message("<div class=\"alert alert-success\">
                                        <strong>Success!</strong> Data is deleted successfully.
                                   </div>");
            header("Location: index.php");
        } else {
            Message::Message("<div class=\"alert alert-danger\">
                                        <strong>Warning!</strong> Data is not deleted.
                                   </div>");
            header("Location: index.php");
        }
    }

    public function trash()
    {
        $sql = "UPDATE `gender` SET `isTrash`=1 WHERE `gender`.`id`=:id";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(":id" => $this->id));
        if ($result) {
            Message::Message("<div class=\"alert alert-success\">
                                        <strong>Success!</strong> Data is Trashed successfully.
                                   </div>");
            header("Location: index.php");
        } else {
            Message::Message("<div class=\"alert alert-danger\">
                                        <strong>Warning!</strong> Error Occurred.
                                   </div>");
            header("Location: index.php");
        }
    }

    public function recycleBin()
    {
        $sql = "SELECT * FROM `gender` WHERE `isTrash`=1";
        $getSql = $this->conn->query($sql);
        $result = $getSql->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function restore()
    {
        $sql = "UPDATE `gender` SET `isTrash`=0 WHERE `gender`.`id`=:id";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(":id" => $this->id));
        if ($result) {
            Message::Message("<div class=\"alert alert-success\">
                                        <strong>Success!</strong> Data is restored successfully.
                                   </div>");
            header("Location: index.php");
        } else {
            Message::Message("<div class=\"alert alert-danger\">
                                        <strong>Warning!</strong> Error Occurred.
                                   </div>");
            header("Location: index.php");
        }
    }
}
