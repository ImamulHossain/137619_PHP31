<?php
namespace App\BITM\SEIP137619\Birthday;
use App\BITM\SEIP137619\Message\Message;
use PDO;

class Birthday
{
    public $id = "";
    public $name = "";
    public $birthday = "";
    public $host = "localhost";
    public $db = "atomicprojects";
    public $user = "atomic";
    public $pass = "123";
    public $conn;

    public function __construct()
    {
        try {
            $this->conn = new PDO("mysql:host=$this->host;dbname=$this->db", $user = $this->user, $pass = $this->pass);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function setData($data = "")
    {
        if (array_key_exists('name', $data) and !empty($data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('birthday', $data) and !empty($data)) {
            $this->birthday = $data['birthday'];
        }
        if (array_key_exists('id', $data) and !empty($data)) {
            $this->id = $data['id'];
        }
    }

    public function store()
    {
        $sql = "INSERT INTO `birthday`(`name`, `birthday`) VALUES (:myName, :myBirthday)";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(":myName" => $this->name, ":myBirthday" => $this->birthday));
        if ($result) {
            Message::Message("<div class=\"alert alert-success\">
                                        <strong>Success!</strong> Data is inserted successfully.
                                   </div>");
            header("Location: index.php");
        } else {
            Message::Message("<div class=\"alert alert-danger\">
                                        <strong>Warning!</strong> Data is not inserted successfully.
                                   </div>");
            header("Location: index.php");
        }
    }

    public function index()
    {
        $sql = "SELECT * FROM `birthday` WHERE `isTrash`=0";
        $getSql = $this->conn->query($sql);
        $result = $getSql->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function view()
    {
        $sql = "SELECT * FROM `birthday` WHERE `id`=:getId";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(":getId" => $this->id));
        $singleResult = $getSql->fetch(PDO::FETCH_ASSOC);
        return $singleResult;
    }

    public function update()
    {
        $sql = "UPDATE `birthday` SET `name`=:setName,`birthday`=:setBirthday WHERE `birthday`.`id`=:id";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(":setName" => $this->name, ":setBirthday" => $this->birthday, ":id" => $this->id));
        if ($result) {
            Message::Message("<div class=\"alert alert-success\">
                                        <strong>Success!</strong> Data is updated successfully.
                                   </div>");
            header("Location: index.php");
        } else {
            Message::Message("<div class=\"alert alert-danger\">
                                        <strong>Warning!</strong> Error Occurred.
                                   </div>");
            header("Location: index.php");
        }

    }
    public function delete(){
        $sql="DELETE FROM `birthday` WHERE `id`=:id";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(":id" => $this->id));
        if ($result) {
            Message::Message("<div class=\"alert alert-success\">
                                        <strong>Success!</strong> Data is deleted successfully.
                                   </div>");
            header("Location: index.php");
        } else {
            Message::Message("<div class=\"alert alert-danger\">
                                        <strong>Warning!</strong> Data is not deleted.
                                   </div>");
            header("Location: index.php");
        }
    }
    public function trash()
    {
        $sql = "UPDATE `birthday` SET `isTrash`=1 WHERE `birthday`.`id`=:id";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(":id" => $this->id));
        if ($result) {
            Message::Message("<div class=\"alert alert-success\">
                                        <strong>Success!</strong> Data is Trashed successfully.
                                   </div>");
            header("Location: index.php");
        } else {
            Message::Message("<div class=\"alert alert-danger\">
                                        <strong>Warning!</strong> Error Occurred.
                                   </div>");
            header("Location: index.php");
        }
    }

    public function recycleBin()
    {
        $sql = "SELECT * FROM `birthday` WHERE `isTrash`=1";
        $getSql = $this->conn->query($sql);
        $result = $getSql->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function restore()
    {
        $sql = "UPDATE `birthday` SET `isTrash`=0 WHERE `birthday`.`id`=:id";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(":id" => $this->id));
        if ($result) {
            Message::Message("<div class=\"alert alert-success\">
                                        <strong>Success!</strong> Data is restored successfully.
                                   </div>");
            header("Location: index.php");
        } else {
            Message::Message("<div class=\"alert alert-danger\">
                                        <strong>Warning!</strong> Error Occurred.
                                   </div>");
            header("Location: index.php");
        }
    }
}
