<?php

namespace App\BITM\SEIP137619\ProfilePicture;
use App\BITM\SEIP137619\Message\Message;
use PDO;
class profilePicture
{

    public $id = "";
    public $name = "";
    public $photo = "";
    public $host = "localhost";
    public $db = "atomicprojects";
    public $user = "atomic";
    public $pass = "123";
    public $conn;

    public function __construct()
    {
        try {
            $this->conn = new PDO("mysql:host=$this->host;dbname=$this->db", $user = $this->user, $pass = $this->pass);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function setData($data = "")
    {
        if (array_key_exists('name', $data) and !empty($data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('photo', $data) and !empty($data)) {
            $this->photo = $data['photo'];
        }
        if (array_key_exists('id', $data) and !empty($data)) {
            $this->id = $data['id'];
        }
    }

    public function store()
    {
        $sql = "INSERT INTO `profilephoto`(`name`, `photo`) VALUES (:myName, :myPhoto)";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(":myName" => $this->name, ":myPhoto" => $this->photo));
        if ($result) {
            Message::Message("<div class=\"alert alert-success\">
                                        <strong>Success!</strong> Data is inserted successfully.
                                   </div>");
            header("Location: index.php");
        } else {
            Message::Message("<div class=\"alert alert-danger\">
                                        <strong>Warning!</strong> Data is not inserted successfully.
                                   </div>");
            header("Location: index.php");
        }
    }

    public function index()
    {
        $sql = "SELECT * FROM `profilephoto`";
        $getSql = $this->conn->query($sql);
        $result = $getSql->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function view()
    {
        $sql = "SELECT * FROM `profilephoto` WHERE `id`=:getId";
        $getSql = $this->conn->prepare($sql);
        $result = $getSql->execute(array(":getId" => $this->id));
        $singleResult = $getSql->fetch(PDO::FETCH_ASSOC);
        return $singleResult;
    }

    public function update()
    {
        if (!empty($this->photo)){
            $sql = "UPDATE `profilephoto` SET `name`=:setName,`photo`=:setPhoto WHERE `profilephoto`.`id`=:id";
            $getSql = $this->conn->prepare($sql);
            $result = $getSql->execute(array(":setName" => $this->name, ":setPhoto" => $this->photo, ":id" => $this->id));

        }
        else{
            $sql = "UPDATE `profilephoto` SET `name`=:setName WHERE `profilephoto`.`id`=:id";
            $getSql = $this->conn->prepare($sql);
            $result = $getSql->execute(array(":setName" => $this->name, ":id" => $this->id));
        }

        if ($result) {
            Message::Message("<div class=\"alert alert-success\">
                                        <strong>Success!</strong> Data is updated successfully.
                                   </div>");
            header("Location: index.php");
        } else {
            Message::Message("<div class=\"alert alert-danger\">
                                        <strong>Warning!</strong> Error Occurred.
                                   </div>");
            header("Location: index.php");
        }

    }
}