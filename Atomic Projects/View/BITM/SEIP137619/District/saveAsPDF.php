<?php
include("../../../../vendor/autoload.php");
use App\BITM\SEIP137619\District\District;
use App\BITM\SEIP137619\Message\Message;

session_start();
$district = new District();
$allDistricts = $district->index();
$sl = 0;
$trs = "";
foreach ($allDistricts as $singleDistrict):
    $sl++;
    $trs .= "<tr>";
    $trs .= "<td>$sl</td>";
    $trs .= "<td>$singleDistrict[id]</td>";
    $trs .= "<td>$singleDistrict[district]</td>";
    $trs .= "</tr>";
endforeach;

$pdfList = <<<saveAsPDF
 <table border="1">
   <thead>
   <tr>
       <th>Serial No</th>
       <th>ID No</th>
       <th>District</th>
       </tr>
   </thead>
   <tbody>
   $trs
   </tbody>
 </table>
saveAsPDF;

include("../../../../vendor/mpdf/mpdf/mpdf.php");
$mpdf = new Mpdf();
$mpdf->WriteHTML($pdfList);
$mpdf->Output();
?>

