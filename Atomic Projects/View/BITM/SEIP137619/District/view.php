<?php
include("../../../../vendor/autoload.php");
use App\BITM\SEIP137619\District\District;

$district = new District();
$district->setData($_GET);
$singleDistrict = $district->view();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Single District</title>

    <!-- Bootstrap -->
    <link href="../../../../Assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../../Assets/css/form.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="backgroundColor">
<section class="container">
    <br><br>
    <div class="row">
        <div class="col-md-5 col-md-offset-3">
            <fieldset>
                <legend>Single District Info</legend>
                <div class="row formBackground">
                    <div class="col-md-10 col-md-offset-1">
                        <br>
                        <div class="form-group">
                            <label for="name">ID:</label>
                            <li class="form-control"><?php echo $singleDistrict['id'] ?></li>
                        </div>
                        <div class="form-group">
                            <label for="district">District:</label>
                            <li class="form-control"><?php echo $singleDistrict['district'] ?></li>
                        </div>
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <form action="delete.php" method="post">
                                    <a href="create.php" role="button" class="btn btn-primary btn-sm">Add New</a>
                                    <a href="edit.php?id=<?php echo $singleDistrict['id']; ?>"
                                       class="btn btn-info btn-sm" role="button">Edit</a>
                                    <a href="trash.php?id=<?php echo $singleDistrict['id']; ?>"
                                       class="btn btn-warning btn-sm" role="button">Trash</a>
                                    <input type="hidden" name="id" value="<?php echo $singleDistrict['id']; ?>">
                                    <button type="submit" class="btn btn-danger btn-sm"
                                            Onclick="return ConfirmDelete();">Delete
                                    </button>
                                </form>
                                <br><br>
                            </div>
                        </div>
                    </div>
                </div>
            </fieldset>
        </div>
    </div>
</section>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../../../Assets/js/bootstrap.min.js"></script>
<script>
    function ConfirmDelete() {
        var x = confirm("Are you sure to delete?");
        if (x)
            return true;
        else
            return false;
    }
</script>
</body>
</html>
