<?php
include("../../../../vendor/autoload.php");
use App\BITM\SEIP137619\CompanyInfo\Company;
use App\BITM\SEIP137619\Message\Message;

session_start();
$company=new Company();
$allCompany = $company->index();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>All Companies List</title>

    <!-- Bootstrap -->
    <link href="../../../../Assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../../Assets/css/form.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="backgroundColor">
<section class="container">
    <br>
    <div class="row headerRow">
        <div class="col-md-1 col-md-offset-1">
            <a href="create.php" class="btn btn-primary" role="button">Add New</a>
        </div>
        <div class="col-md-1">
            <a href="recycleBin.php" class="btn btn-warning" role="button">Recycle Bin</a>
        </div>
        <div class="col-md-6 messageShow">
            <div id="message">
                <?php
                if (array_key_exists('giveMessage', $_SESSION) and !empty($_SESSION['giveMessage'])) {
                    echo Message::message();
                }
                ?>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <legend>All Companies List</legend>
            <table class="table table-hover table-striped tableClass">
                <thead>
                <th>Serial No</th>
                <th>ID No</th>
                <th>Company Name</th>
                <th>Description</th>
                <th>Actions</th>
                </thead>

                <tbody>
                <?php
                $sl = 0;
                foreach ($allCompany as $company) {
                    $sl++;
                    ?>
                    <tr>
                        <td><?php echo $sl ?></td>
                        <td><?php echo $company['id'] ?></td>
                        <td><?php echo $company['companyName'] ?></td>
                        <td class="indexDescription"><?php echo $company['description'] ?></td>
                        <td>
                            <form action="delete.php" method="post">
                                <a href="view.php?id=<?php echo $company['id']; ?>"
                                   class="btn btn-primary btn-xs" role="button">View</a>
                                <a href="edit.php?id=<?php echo $company['id']; ?>"
                                   class="btn btn-info btn-xs" role="button">Edit</a>
                                <a href="trash.php?id=<?php echo $company['id']; ?>"
                                   class="btn btn-warning btn-xs" role="button">Trash</a>
                                <input type="hidden" name="id" value="<?php echo $company['id']; ?>">
                                <button type="submit" class="btn btn-danger btn-xs"
                                        Onclick="return ConfirmDelete();">Delete
                                </button>
                            </form>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
</section>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../../../Assets/js/bootstrap.min.js"></script>
<script>
    $("#message").show().delay(3000).fadeOut();
    function ConfirmDelete() {
        var x = confirm("Are you sure to delete?");
        if (x)
            return true;
        else
            return false;
    }
</script>
</body>
</html>