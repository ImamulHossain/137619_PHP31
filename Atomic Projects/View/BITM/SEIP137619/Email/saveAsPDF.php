<?php
include("../../../../vendor/autoload.php");
use App\BITM\SEIP137619\Email\Email;

$email = new Email();
$allEmails = $email->index();
$sl = 0;
$trs = "";
foreach ($allEmails as $singleEmail):
    $sl++;
    $trs .= "<tr>";
    $trs .= "<td>$sl</td>";
    $trs .= "<td>$singleEmail[id]</td>";
    $trs .= "<td>$singleEmail[name]</td>";
    $trs .= "<td>$singleEmail[email]</td>";
    $trs .= "</tr>";
endforeach;

$pdfList = <<<saveAsPDF


<table border="1">
   <thead>
       <tr>
           <th>Serial No</th>
           <th>ID No</th>
           <th>Name</th>
           <th>Email Address</th>
        </tr>
   </thead>
   <tbody>
   $trs
   </tbody>
</table>

saveAsPDF;

include("../../../../vendor/mpdf/mpdf/mpdf.php");
$mpdf = new Mpdf();
$mpdf->WriteHTML($pdfList);
$mpdf->Output();