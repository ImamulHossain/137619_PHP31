<?php
include("../../../../vendor/autoload.php");
use App\BITM\SEIP137619\Mobile\Mobile;
use App\BITM\SEIP137619\Message\Message;
session_start();
$mobile = new Mobile();
$mobile->setData($_GET);
$singleMobile = $mobile->view();
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Edit Mobile</title>

    <!-- Bootstrap -->
    <link href="../../../../Assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../../Assets/css/form.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="backgroundColor">
<section class="container">
    <br><br>
    <div class="row">
        <div class="col-md-5 col-md-offset-3">
            <form action="update.php" method="post">
                <fieldset>
                    <legend>Update Mobile Info</legend>
                    <div class="row formBackground">
                        <div class="col-md-10 col-md-offset-1">
                            <br>
                            <div class="form-group">
                                <input type="hidden" name="id" value="<?php echo $singleMobile['id'] ?>">
                                <label for="name">Name:</label>
                                <input type="text" name="name" value="<?php echo $singleMobile['name'] ?>"
                                       class="form-control" id="name">
                            </div>
                            <div class="form-group">
                                <label for="mobile">Mobile:</label>
                                <input type="text" name="mobile" value="<?php echo $singleMobile['mobile'] ?>"
                                       class="form-control" id="mobile">
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary btn-sm">Update</button>
                                    <button type="reset" class="btn btn-info btn-sm">Reset</button>
                                    <br><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</section>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../../../Assets/js/bootstrap.min.js"></script>
</body>
</html>
