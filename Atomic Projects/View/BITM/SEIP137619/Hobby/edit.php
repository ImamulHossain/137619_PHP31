<?php
include("../../../../vendor/autoload.php");
use App\BITM\SEIP137619\Hobby\Hobby;
use App\BITM\SEIP137619\Message\Message;

session_start();
$hobby = new Hobby();
$hobby->setData($_GET);
$singleHobby = $hobby->view();
$hobbyInString = $singleHobby['hobby'];
$hobbyInArray = explode(",", $hobbyInString);
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Edit Hobby</title>

    <!-- Bootstrap -->
    <link href="../../../../Assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../../Assets/css/form.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="backgroundColor">
<section class="container">
    <br><br>

    <div class="row">
        <div class="col-md-5 col-md-offset-3">
            <form action="update.php" method="post">
                <fieldset>
                    <legend>Update Hobby Info</legend>
                    <div class="row formBackground">
                        <div class="col-md-10 col-md-offset-1">
                            <br>

                            <div class="form-group">
                                <input type="hidden" name="id" value="<?php echo $singleHobby['id'] ?>">
                                <label for="name">Name:</label>
                                <input type="text" name="name" value="<?php echo $singleHobby['name'] ?>"
                                       class="form-control" id="name">
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label for="hobby">Hobby:</label>

                                        <div class="checkbox">
                                            <label><input type="checkbox" value="C" name="hobby[]" <?php if (in_array("C", $hobbyInArray)) {
                                                    echo "checked";
                                                } ?>>C</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" value="C++" name="hobby[]"<?php if (in_array("C++", $hobbyInArray)) {
                                                    echo "checked";
                                                } ?>>C++</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" value="C#" name="hobby[]"<?php if (in_array("C#", $hobbyInArray)) {
                                                    echo "checked";
                                                } ?>>C#</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" value="Dot Net" name="hobby[]"<?php if (in_array("Dot Net", $hobbyInArray)) {
                                                    echo "checked";
                                                } ?>>Dot Net</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group"><br>

                                        <div class="checkbox">
                                            <label><input type="checkbox" value="PHP" name="hobby[]"<?php if (in_array("PHP", $hobbyInArray)) {
                                                    echo "checked";
                                                } ?>>PHP</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" value="HTML" name="hobby[]"<?php if (in_array("HTML", $hobbyInArray)) {
                                                    echo "checked";
                                                } ?>>HTML</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input type="checkbox" value="CSS" name="hobby[]"<?php if (in_array("CSS", $hobbyInArray)) {
                                                    echo "checked";
                                                } ?>>CSS</label>
                                        </div>
                                        <div class="checkbox">
                                            <label><input
                                                    type="checkbox" value="Java Script" name="hobby[]"<?php if (in_array("Java Script", $hobbyInArray)) {
                                                    echo "checked";
                                                } ?>>Java Script</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 col-md-offset-4">
                                <button type="submit" class="btn btn-primary btn-sm">Update</button>
                                <button type="reset" class="btn btn-info btn-sm">Reset</button>
                                <br><br>
                            </div>
                        </div>
                    </div>
        </div>
        </fieldset>
        </form>
    </div>
    </div>
</section>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../../../Assets/js/bootstrap.min.js"></script>
</body>
</html>
