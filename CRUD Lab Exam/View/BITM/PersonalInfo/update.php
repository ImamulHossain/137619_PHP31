<?php
include("../../../vendor/autoload.php");
use App\BITM\PersonalInfo\PersonalInfo;

$hobbyInString = implode(",", $_POST["Hobby"]);
$_POST["Hobby"] = $hobbyInString;

$person = new PersonalInfo();
$person->setData($_POST);
$person->update();
