<?php
include("../../../vendor/autoload.php");
use App\BITM\PersonalInfo\PersonalInfo;

$person = new PersonalInfo();
$person->setData($_GET);
$allResult = $person->view();

$hobbyInString = $allResult["hobby"];
$hobbyInArray = explode(",", $hobbyInString);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Personal Info</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <form method="post" action="update.php">
        <div class="row">
            <div class="col-md-7 col-md-offset-2">
                <h2>Personal History Form</h2>
                <div class="form-group">
                    <input type ="hidden" name="id" value="<?php echo $allResult['id']?>" >
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" id="name" autofocus required name="name"
                           value="<?php echo $allResult['name']; ?>">
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" id="email" autofocus required name="email"
                           value="<?php echo $allResult['email']; ?>">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-md-offset-2">
                <div class="form-group">
                    <label for="gender">Gender:</label><br>
                    <label class="radio-inline"><input type="radio" name="gender"
                                                       value="Male" <?php if (in_array("Male", $allResult)) {
                            echo "checked";
                        } ?>>Male</label>
                    <label class="radio-inline"><input type="radio" name="gender"
                                                       value="Female" <?php if (in_array("Female", $allResult)) {
                            echo "checked";
                        } ?>>Female</label>
                    <label class="radio-inline"><input type="radio" name="gender"
                                                       value="Other" <?php if (in_array("Other", $allResult)) {
                            echo "checked";
                        } ?>>Other</label>
                </div>
            </div>
            <div class="col-md-3 col-md-offset-1">
                <div class="form-group">
                    <label>Hobby:</label><br>
                    <label class="checkbox-inline"><input type="checkbox" value="C++"
                                                          name="Hobby[]" <?php if (in_array("C++", $hobbyInArray)) {
                            echo "checked";
                        } ?>>C++</label>
                    <label class="checkbox-inline"><input type="checkbox" value="C#" name="Hobby[]" <?php if (in_array("C#", $hobbyInArray)) {
                            echo "checked";
                        } ?>>C#</label>
                    <label class=" checkbox-inline"><input type="checkbox" value="PHP" name="Hobby[]" <?php if (in_array("PHP", $hobbyInArray)) {
                            echo "checked";
                        } ?>>PHP</label>
                    <label class=" checkbox-inline"><input type="checkbox" value="Java" name="Hobby[]" <?php if (in_array("Java", $hobbyInArray)) {
                            echo "checked";
                        } ?>>Java</label>
                </div>
            </div>
        </div>
        <div class=" row">
            <div class="col-md-2 col-md-offset-5">
                <div class="form-group">
                    <input type="submit" value="Update" class="btn btn-primary">
                </div>
            </div>
        </div>
    </form>
</div>
</body>
</html>



