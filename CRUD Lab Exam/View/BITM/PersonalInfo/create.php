<!DOCTYPE html>
<html lang="en">
<head>
    <title>Personal Info</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <form method="post" action="store.php">
        <div class="row">
            <div class="col-md-7 col-md-offset-2">
                <h2>Personal History Form</h2>
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" class="form-control" id="name" autofocus required name="name">
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control" id="email" autofocus required name="email">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3 col-md-offset-2">
                <div class="form-group">
                    <label for="gender">Gender:</label><br>
                    <label class="radio-inline"><input type="radio" name="gender" value="Male">Male</label>
                    <label class="radio-inline"><input type="radio" name="gender" value="Female">Female</label>
                    <label class="radio-inline"><input type="radio" name="gender" value="Other">Other</label>
                </div>
            </div>
            <div class="col-md-3 col-md-offset-1">
                <div class="form-group">
                    <label>Hobby:</label><br>
                    <label class="checkbox-inline"><input type="checkbox" value="C++" name="Hobby[]">C++</label>
                    <label class="checkbox-inline"><input type="checkbox" value="C#" name="Hobby[]">C#</label>
                    <label class=" checkbox-inline"><input type="checkbox" value="PHP" name="Hobby[]">PHP</label>
                    <label class=" checkbox-inline"><input type="checkbox" value="Java" name="Hobby[]">Java</label>
                </div>
            </div>
        </div>
        <div class=" row">
            <div class="col-md-2 col-md-offset-5">
                <div class="form-group">
                    <input type="submit" value="Submit" class="btn btn-primary">
                </div>
            </div>
        </div>
    </form>
</div>
</body>
</html>