<?php
namespace App\Bitm\SEIPXXXX\ProfilePicture;
use App\Bitm\SEIPXXXX\Message\Message;
use App\Bitm\SEIPXXXX\Utility\Utility;
use PDO;


class ImageUploader{
    public $id = "";
    public $name = "";
    public $image_name = "";
    public $serverName = "localhost";
    public $databaseName = "atomicproject";
    public $user = "root";
    public $pass = "";
    public $conn;
    public $deleted_at="";

    public function __construct()
    {
        try {
            # MySQL with PDO_MYSQL
            $this->conn = new PDO("mysql:host=$this->serverName;dbname=$this->databaseName", $this->user, $this->pass);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    //$_GET['id']
    public function setData($data = "")
    {
        if (array_key_exists('user_name', $data) and !empty($data)) {
            $this->name = $data['user_name'];
        }
        if (array_key_exists('image', $data) and !empty($data)) {
            $this->image_name = $data['image'];
        }
        if (array_key_exists('id', $data) and !empty($data)) {
            $this->id = $data['id'];
        }
        //var_dump($this);
        return $this;
    }

    public function store()
    {
        $query = "INSERT INTO `profilepicture` (`name`, `imagename`) VALUES (:name, :imagename)";
        $stmt = $this->conn->prepare($query);
        $result = $stmt->execute(array(':name' => $this->name,
            ':imagename' => $this->image_name
        ));
        if ($result) {
            //header('Location:index.php');
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been stored successfully.
</div>");
            Utility::redirect('index.php');
        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Some error has been occured
</div>");
            Utility::redirect('index.php');
        }

    }
    public function index()
    {
        $sqlquery = "SELECT * FROM `profilepicture`";
        $stmt = $this->conn->query($sqlquery);
        $alldata = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $alldata;
    }

    public function view()
    {
        $sqlquery = "SELECT * FROM `profilepicture` WHERE `id`=:setid";
        $stmt = $this->conn->prepare($sqlquery);
        $stmt->execute(array(':setid' => $this->id
        ));
        $singledata = $stmt->fetch(PDO::FETCH_OBJ);
        return $singledata;
    }
    public function update()
    {   if(!empty($this->image_name)){
        $query = "UPDATE `profilepicture` SET `name` = :name, `imagename` = :image WHERE `profilepicture`.`id` = :id";
        $stmt = $this->conn->prepare($query);
        $result = $stmt->execute(array(':name' => $this->name,
            ':image' => $this->image_name,
            ':id' => $this->id
        ));

    }
        else{
            $query = "UPDATE `profilepicture` SET `name` = :name WHERE `profilepicture`.`id` = :id";
            $stmt = $this->conn->prepare($query);
            $result = $stmt->execute(array(':name' => $this->name,
                ':id' => $this->id
            ));

        }

        if ($result) {
            //header('Location:index.php');
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data has been updated successfully.
</div>");
            Utility::redirect('index.php');
        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Some error has been occured
</div>");
            Utility::redirect('index.php');
        }
    }






}
