<?php
namespace App\Bitm\SEIPXXXX\Message;
if(!isset($_SESSION['success_message'])) {
    session_start();
}
class Message{
    public static function message($message=NULL){

        if(is_null($message)){
            $_message=self::getMessage();
            return $_message;
        }

        else {
            self::setMessage($message);
        }

    }


    public static function setMessage($message){
        $_SESSION['success_message']=$message;

    }

    public static function getMessage(){
        $_message=$_SESSION['success_message'];//Data has been stored successfully
        $_SESSION['success_message']="";
        return $_message;

    }


}
