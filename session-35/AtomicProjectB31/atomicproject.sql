-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2016 at 06:01 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomicproject`
--

-- --------------------------------------------------------

--
-- Table structure for table `hobboy`
--

CREATE TABLE `hobboy` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `hobby_list` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `mobile`
--

CREATE TABLE `mobile` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `model_name` varchar(255) NOT NULL,
  `deleted_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mobile`
--

INSERT INTO `mobile` (`id`, `title`, `model_name`, `deleted_at`) VALUES
(16, 'fdgfd', 'fdgfgdf', '2016-11-13'),
(17, 'ddd', 'dddddddddddd', '2016-11-13'),
(19, 'hgfgh', 'hfghfg', '2016-11-13'),
(20, 'new', 'daa', NULL),
(21, 'asdasd', 'dsadasddsa', NULL),
(22, 'sgsdg', 'sdggsdg', NULL),
(23, 'sadasd', 'dasdasdsad', '2016-11-13'),
(24, 'sdaasd', 'dasddsa', NULL),
(25, 'dassad', 'sdasadasd', NULL),
(26, 'hjhgjhj', 'hjhgjgh', NULL),
(29, 'sad', 'dsads', NULL),
(30, 'xx', 'xxx', NULL),
(31, 'hfghfg', 'ghfgh', NULL),
(32, 'sdadasd', 'dsadsa', NULL),
(33, 'fgdfg', 'dgdfgdfd', NULL),
(34, 'sdadsa', 'saddas', NULL),
(35, 'eee', 'eeee', NULL),
(36, 'sss', 'sss', NULL),
(37, 'erwewrrew', 'gdfgd', NULL),
(38, 'sadsad', 'dassadd', NULL),
(39, 'asdsadasd', 'sadsadsaddsads', NULL),
(40, 'ddd', 'ddd', NULL),
(41, 'ddd', 'dddd', NULL),
(42, '', '', NULL),
(43, 'lava', 'e60', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profilepicture`
--

CREATE TABLE `profilepicture` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `imagename` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profilepicture`
--

INSERT INTO `profilepicture` (`id`, `name`, `imagename`) VALUES
(6, 'Bitm12123ffffff', 'kkkk'),
(7, 'dddgggg', ''),
(8, 'new data', '1479789125bitm (2).png'),
(9, 'dddd', '1479789170bitm (12).jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hobboy`
--
ALTER TABLE `hobboy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mobile`
--
ALTER TABLE `mobile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profilepicture`
--
ALTER TABLE `profilepicture`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hobboy`
--
ALTER TABLE `hobboy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mobile`
--
ALTER TABLE `mobile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `profilepicture`
--
ALTER TABLE `profilepicture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
