<?php
session_start();
//echo $_SERVER['DOCUMENT_ROOT'];
include_once($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR."AtomicProjectB31".DIRECTORY_SEPARATOR."vendor".DIRECTORY_SEPARATOR."autoload.php");

use App\Bitm\SEIPXXXX\Message\Message;
use App\Bitm\SEIPXXXX\Utility\Utility;
use \App\Bitm\SEIPXXXX\ProfilePicture\ImageUploader;
$obj= new ImageUploader();
$singleData= $obj->setData($_GET)->view();
//Utility::dd($singleData);


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <div id="message">
        <?php
        if(array_key_exists('success_message',$_SESSION) and !empty($_SESSION['success_message'])){
            echo Message::message();
        }
        ?>
    </div>
    <h2>Profile picture edit form</h2>

    <form action="update.php" method="post" enctype="multipart/form-data">


        <div class="form-group">

            <input type="hidden" name="id" value="<?php echo $singleData->id?>">
            <label for="title">Name:</label>
            <input type="text" class="form-control" id="title" name="user_name" value="<?php echo $singleData->name ?>"
                   placeholder="Enter your name">
        </div>
        <div class="form-group">
            <label for="model_name">Profile Picture:</label>
            <input type="file" class="form-control" id="model_name" name="image">
            <img src="../../../Resource/ImageStorage/<?php echo $singleData->imagename?>" alt="Mountain View" style="width:100px;height:100px;">
        </div>


        <button type="submit" class="btn btn-default">Submit</button>
    </form>
</div>
</div>

</body>
</html>
