<?php

include_once('../../../vendor/autoload.php');
use \App\Bitm\SEIPXXXX\ProfilePicture\ImageUploader;
use App\Bitm\SEIPXXXX\Utility\Utility;
use App\Bitm\SEIPXXXX\Message\Message;

//Utility::dd($_FILES);
//Utility::dd($_POST);
if(isset($_FILES['image']) && !empty($_FILES['image']["name"])){
        $imageName= time().$_FILES["image"]["name"];
        $tempLocation= $_FILES["image"]["tmp_name"];
        move_uploaded_file($tempLocation,"../../../Resource/ImageStorage/".$imageName);
        $_POST['image']=$imageName;
        //Utility::dd($_POST);

}

if(isset($_POST['image']) && !empty($_POST['image'])){
    $obj= new ImageUploader();
    $obj->setData($_POST)->store();

}
else {
    Message::message("Please input some data");
    Utility::redirect("create.php");

}

